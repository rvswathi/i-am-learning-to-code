# sum of range of values that are divisible by 3 or 5

print(sum([x for x in range(1,1000) if x % 3 * x % 5 == 0]))