# sum of unique numbers from a given list

li = [2,3,4,5,2,56,77,8,9,8]
print(sum([x for x in li if li.count(x) == 1])) 